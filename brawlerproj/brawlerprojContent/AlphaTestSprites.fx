


sampler Samp0 : s0;
Texture2D Tex0 : t0;

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float4 Color0 : COLOR0;
    float2 TC0 : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float2 TC0 : TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float2 vpMax = float2(800.0f, 600.0f);
    float2 vpMin = float2(0.0f, 0.0f);

    //clipspace
    float2 csMax = float2(1.0f, 1.0f);
    float2 csMin = float2(-1.0f, -1.0f);

    float2 normalised = (input.Position.xy - float2(0.5f,0.5f) - vpMin) / (vpMax - vpMin);
    output.Position.xy = ((normalised * (csMax - csMin)) + csMin )* float2(1.0f, -1.0f);

    //output.Position.x = (input.Position.x * 2.0f / viewportMax.x) - 1.0f;
    //output.Position.y = (input.Position.y * 2.0f / viewportMax.y) - 1.0f;
    output.Position.z = input.Position.z;
    output.Position.w = 1.0f;
    output.TC0 = input.TC0;
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float4 texCol = tex2D(Samp0, input.TC0);

    if (texCol.a <= 0.0f)
    {
        discard;
    }
    return texCol;

}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
