﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using Microsoft.Xna.Framework;

namespace brawlerproj
{
    static class Gleed2DLevelImporter
    {

        //gleed 2d coord system has (0,0) in top left.
        //YTop = in viewspace when looking at a gleed2d map in the editor, what is the "topmost" Y position?
        //YBottom = bottommost Y.
        public static WPYExtents GetYExtents(Gleed2D.InGame.Level gleedLevel)
        {
            WPYExtents YExtents;
            if (gleedLevel.Properties.CustomProperties.Count > 1 &&
                !gleedLevel.Properties.CustomProperties.ContainsKey("WalkPlaneDimensions"))
            {
                throw new MapLoadException("Level root must have Vec2 WalkPlaneDimensions property.");
            }
            Log.Instance.Print(DPFCLASS.GLEED2D, "game root customProps: {0}", Utility.DictToString(gleedLevel.Properties.CustomProperties));

            Gleed2D.InGame.CustomProperty customProp = gleedLevel.Properties.CustomProperties["WalkPlaneDimensions"];
            Debug.Assert(customProp.Type == typeof(Vector2));
            Vector2 YExtentsVec = (Vector2)customProp.Value;

            YExtents.YTop = Math.Min(YExtentsVec.X, YExtentsVec.Y);
            YExtents.YBottom = Math.Max(YExtentsVec.X, YExtentsVec.Y);

            Log.Instance.Print(DPFCLASS.GLEED2D, "%\t\t\tInit " + YExtents);

            return YExtents;
        }

        public static List<MapThingy> GetMapItemListFromGleedLayer(Game1 game1, Gleed2D.InGame.Layer layer, WPYExtents YExtents)
        {
            Log.Instance.Print(DPFCLASS.GLEED2D, "=\tGetMapItemListFromGleedLayer: {0} items", (new List<Gleed2D.InGame.LayerItem>(layer.Items)).Count);

            List<MapThingy> mapItems = new List<MapThingy>();

            foreach (Gleed2D.InGame.LayerItem item in layer.Items)
            {
                Log.Instance.Print(DPFCLASS.GLEED2D, "#\t\tlayer_item ID:{0} name:{1} position:{2} visible:{3} custom props:{4} type:{5} type:{6}",
                    item.Properties.Id, item.Properties.Name, item.Properties.Position,
                    item.Properties.Visible,
                    Utility.DictToString(item.Properties.CustomProperties, null),
                    item.GetType(),
                    item.Properties.GetType());
                Debug.Assert(item.Properties.CustomProperties.Count == 0);

                System.Type type = item.Properties.GetType();

                if (type == typeof(Gleed2D.InGame.TextureItemProperties))
                {
                    Gleed2D.InGame.TextureItemProperties texProp = item.Properties as Gleed2D.InGame.TextureItemProperties;
                    mapItems.Add(new TexturedRectThingy(game1, texProp));
                }
                else if (type == typeof(Gleed2D.InGame.RectangleItemProperties))
                {
                    Gleed2D.InGame.RectangleItemProperties rectProp = item.Properties as Gleed2D.InGame.RectangleItemProperties;

                    string fmt = String.Format("#\t\t\tRect: Position:{0} Height:{1} Width:{2}",
                        rectProp.Position, rectProp.Height, rectProp.Width);
                    Log.Instance.Print(DPFCLASS.GLEED2D, fmt);

                    ClipRectangle clipRect = new ClipRectangle();
                    clipRect.InitClipRectangle(YExtents, rectProp);
                    mapItems.Add(clipRect);
                }
                else if (type == typeof(Gleed2D.InGame.PathItemProperties))
                {
                    Gleed2D.InGame.PathItemProperties pathProp = item.Properties as Gleed2D.InGame.PathItemProperties;

                    string fmt = String.Format("#\t\t\tPath: Position:{0} Poly?{1} Locals:[{2}] World:[{3}]",
                        pathProp.Position,
                        pathProp.IsPolygon,
                        String.Join<Vector2>(", ", pathProp.LocalPoints),
                        String.Join<Vector2>(", ", pathProp.WorldPoints));
                    Log.Instance.Print(DPFCLASS.GLEED2D, fmt);

                    ClipTriangle clipTri = new ClipTriangle();
                    clipTri.InitClipTriangle(YExtents, pathProp);
                    mapItems.Add(clipTri);
                }
                else
                {
                    throw new MapLoadException("unknown item properties type " + type.ToString());
                }
                //Gleed2D.InGame.CustomProperty
            }
            return mapItems;
        }

        public static void InitClipTriangle(this ClipTriangle clipTri, WPYExtents YExtents, Gleed2D.InGame.PathItemProperties pathProp)
        {
            Debug.Assert(pathProp.CustomProperties.Count == 0);
            Debug.Assert(pathProp.LineColor == new Color(192, 0, 192, 145));
            Debug.Assert(pathProp.LineWidth == 4); //or whatever the default is

            //Debug.Assert(pathProp.Position
            //Debug.Assert(pathProp.LocalPoints
            //Debug.Assert(pathProp.Name
            //Debug.Assert(pathProp.Visible

            Debug.Assert(pathProp.WorldPoints.Count == 3 && pathProp.IsPolygon);
            Debug.Assert(pathProp.Position + pathProp.LocalPoints[0] == pathProp.WorldPoints[0]);

            Vector2[] triPoints = pathProp.WorldPoints.ToArray();
            Debug.Assert(triPoints.Rank == 1 && triPoints.GetLength(0) == clipTri.Vertices.GetLength(0));

            for (int i = 0; i < clipTri.Vertices.Length; i++)
            {
                Vector2 VtxScreen = triPoints[i];
                float screenY = Map.ScreenYToWorldZ(YExtents, VtxScreen.Y);
                clipTri.Vertices[i].X = VtxScreen.X;
                clipTri.Vertices[i].Y = 0.0f;
                clipTri.Vertices[i].Z = screenY;
            }

            //It should always be the case that [0].Y == [1].Y == [2].Y -- i.e. axis aligned.
            Log.Instance.Print(DPFCLASS.GLEED2D, "%\t\t\tInit " + clipTri);
        }


        public static void InitClipRectangle(this ClipRectangle clipRect, WPYExtents YExtents, Gleed2D.InGame.RectangleItemProperties rectProp)
        {
            Debug.Assert(rectProp.FillColor == new Color(192, 0, 192, 145));
            Debug.Assert(rectProp.Rotation == 0.0f);
            Debug.Assert(rectProp.CustomProperties.Count == 0);
            //Id
            //Name
            //Visible

            //Gleed's rectangles use float positions and width/heights.
            Debug.Assert(System.Math.Truncate(rectProp.Position.X) == rectProp.Position.X);
            Debug.Assert(System.Math.Truncate(rectProp.Position.Y) == rectProp.Position.Y);
            Debug.Assert(System.Math.Truncate(rectProp.Width) == rectProp.Width);
            Debug.Assert(System.Math.Truncate(rectProp.Height) == rectProp.Height);

            //screenX and worldX match. As does the width
            float wX = rectProp.Position.X;
            float wWidth = rectProp.Width;

            //screenY needs to become world Z, for which the walk plane is in the range 0.0-1.0;
            //screen height needs to be scaled from 0-height to Z plane scale
            float wZ = Map.ScreenYToWorldZ(YExtents, rectProp.Position.Y);
            float wHeightUnadjust = Map.ScreenYToWorldZ(YExtents, rectProp.Position.Y + rectProp.Height);
            float wHeight = wHeightUnadjust - wZ;
            Debug.Assert(wHeight > 0.0f);
            //Debug.Assert(System.Math.Truncate(wY) == wY);

//#error
            //wZ and wHeight currently stored *1000 to avoid the horrible rounding destroyed by ftoi.
            clipRect.XZPlane = new Rectangle((int)wX, (int)(wZ * Utility.INT_RECT_SUCKS), (int)wWidth, (int)(wHeight * Utility.INT_RECT_SUCKS));
            clipRect.YPosition = 0.0f;
            Log.Instance.Print(DPFCLASS.GLEED2D, "%\t\t\tInit " + clipRect);
        }

        public static void InitMapFromGleed2D(this Map map, Gleed2D.InGame.Level gleedLevel)
        {
            //gleed 2d coord system has (0,0) in top left.
            //YTop = in viewspace when looking at a gleed2d map in the editor, what is the "topmost" Y position?
            //YBottom = bottommost Y.
            map.WalkPlane.YExtents = Gleed2DLevelImporter.GetYExtents(gleedLevel);


            // level.Properties contains nothing useful
            // layer.Properties.Id is always 0?
            foreach (Gleed2D.InGame.Layer layer in gleedLevel.Layers)
            {
                Log.Instance.Print(DPFCLASS.GLEED2D, "=\tlayer ID:{0} Name:{1} Position:{2} ScrollSpeed:{3} Visible:{4} CustomProps:{5}",
                    layer.Properties.Id, layer.Properties.Name, layer.Properties.Position,
                    layer.Properties.ScrollSpeed, layer.Properties.Visible,
                    Utility.DictToString(layer.Properties.CustomProperties, null));

                Debug.Assert(layer.Properties.CustomProperties.Count == 0);

                List<MapThingy> mapItems = Gleed2DLevelImporter.GetMapItemListFromGleedLayer(map.Game1, layer, map.WalkPlane.YExtents);

                string layerName = layer.Properties.Name;
                Debug.Assert(!string.IsNullOrWhiteSpace(layerName));

                if (layerName == "central" ||
                    layerName == "walkplane")
                {
                    //there can be only one
                    map.WalkPlane.AddMapItemsToWalkPlane(mapItems);
                }
                else if (layerName.StartsWith("foreground_"))
                {
                    string numeralString = layerName.Remove(0, "foreground_".Length);
                    int numeral = System.Int32.Parse(numeralString);

                    var fglayer = new MapParallaxLayer(mapItems, numeral);
                    map.ForegroundLayers.Add(fglayer);
                }
                else if (layerName.StartsWith("background_"))
                {
                    string numeralString = layerName.Remove(0, "background_".Length);
                    int numeral = System.Int32.Parse(numeralString);

                    var bglayer = new MapParallaxLayer(mapItems, numeral);
                    map.BackgroundLayers.Add(bglayer);
                }
                else
                {
                    throw new MapLoadException("Layer name does not belong to a valid layer group:" + layerName);
                }

            }

            map.BackgroundLayers.Sort();
            map.ForegroundLayers.Sort();
        }
    }
}
