using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace brawlerproj
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        //I don't really understand the use of this GraphicsManager
        public GraphicsDeviceManager GraphicsManager { get; set; }
        public SpriteBatch SpriteBatch { get; set; }
        public Background Background { get; set; }
        public Dude PlayerDude { get; set; }


        public Effect MyAlphaTest;

        public Game1()
        {
            this.GraphicsManager = new GraphicsDeviceManager(this);
            this.GraphicsManager.PreferredBackBufferWidth = 800;
            this.GraphicsManager.PreferredBackBufferHeight = 600;

            this.Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.Background = new Background(this);
            Components.Add(this.Background);
            this.PlayerDude = new Dude(this);
            Components.Add(this.PlayerDude);

            //base.Initialize calls LoadContent on this Game and Initialize all the GameComponents,
            //which in turn calls LoadContent on all those components
            //so only call base.Initialize after setting up resources required by the this.LoadContent() function.
            base.Initialize();

            Effect effect = Content.Load<Effect>("AlphaTestSprites");
            this.MyAlphaTest = effect;

        }



        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// 
        /// Called by Game.Initalize
        /// Should not access GraphicsDevice until LoadContent is called.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            // TODO: Add your update logic here

            //base update cycles through items in this.Components and calls update on each of them
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            this.GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.CornflowerBlue, 1.0f, 0);

            // TODO: Add your drawing code here

            //base draw cycles through items in this.Components and calls draw on each of them
            base.Draw(gameTime);
        }


    }
}
