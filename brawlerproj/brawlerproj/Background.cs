﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System;

namespace brawlerproj
{


    /*
    * level is made up of layers.
    *      central walkable layer is the middle area where players walk.
    *      layers behind it are background parallax layers
    *      layers infront it are foreground parallax layers
    *
    *  back- and foreground parallax layers:
    *      each layer has a parallax scroll rate - OR , that rate is defined by which layer it is?
    *      simply consist of textured prims
    *      fore layers might be alpha blended if they obscure a player.
    *      
    * 
    *  central layer is where everything happens.
    *      # visible 'floor' will be simple textured prims
    *      
    *      # invisible prims will define various zones, areas, triggers etc
    *      
    *      # all prims will either be completely none-passable, jump-passable, or all-passable.
    *         = no-pass/all blocking means no entity can go into it, no matter what.
    *          
    *         = jump-passable cannot be walked into, but if a character is 'jumping' then they might pass into/over it?
    *          
    *         = jump-passable areas will need a 'height' to allow a jumping character to rest on them.
    *          i.e. imagine a 'fence'. This will appear to have 'perspective', but the prims defining the blocking area
    *              will be the sprite's area. Naturally the 'further back' part of the fence will be higher up the screen.
    *              So if a player a is to the left of a fence and jumps whilst pushing right he would expect to clip
    *              through or even land 'ontop' of the fence to the right of his position, except the first part of the quad
    *              the player will interact with would be the very 'top' area which is actually furtherst back, so it looks like
    *              he is standing at the far end. Very confusing!
    *              The height is how the game knows where an entity can rest on the block.
    *         = note: this height is because the engine is purely 2d, using perspective and such tricks to give the illusion
    *              of detph.
    *              a proper 3d engine, i.e. one that had an actual XZ plane and tracked proper Y position would have no
    *              such need for a height, as the fence in the world would have a proper Y height defined for it at every
    *              point
    *      
    *      # some prims can be used to define areas that are cliff edges etc, so walking into these areas would trigger the
    *          player to just fall straight down, not interacting with any other prims, until they fall to their death
    *              (or hit a prim defined as being stop-fall or something?)
    *      # some prims can cause damage, kill, push into certain areas etc (so simulate cliffs, lava, oceans, wind,
    *              conveyor belts).
    *      # rocks, fences, trees etc can litter the middle area as textured quads.
    *          they can have seperate clip prims or the clip info can be in the quad. (clip info, height etc described above)
    *      # most of the time a giant quad will cover from the horizon upwards (i.e. the background parralax zone) that is set
    *          to all block. this stops players walking off the top of the walk region and into the background.
    *          = to do platforms or raised areas (think golden eye), the blocking quads can be removed from those areas
    *      # prims can define trigger areas that do ceratin actions (spawn mobs, lock camera to certain area, free,
    *              send GO message, trigger other prims ENABLE or DISABLE state)
    *              
    * entities will have their world 'XZ' plane position tracked and a seperate 'jump height' (or world y) tracked, to allow
    *      them to interact with the jump-passable things properly. And also so that they can only jump-kick people in their
    *      X plane, etc.
    */

    public class MapThingy
    {

    }
    public class TexturedRectThingy : MapThingy
    {
        public Vector2 Position;
        public Texture2D Image;

        public string AssetName;

        //todo should be factored out into Gleed2DLevelImporter.cs, like InitClipRectangle etc
        public TexturedRectThingy(Game game, Gleed2D.InGame.TextureItemProperties texProp)
        {
            Debug.Assert(texProp.CustomProperties.Count == 0);
            Debug.Assert(!texProp.FlipHorizontally);
            Debug.Assert(!texProp.FlipVertically);
            Debug.Assert(!texProp.IsTemplate);
            Debug.Assert(texProp.Rotation == 0.0f);
            Debug.Assert(texProp.Scale == new Vector2(1.0f, 1.0f));
            Debug.Assert(texProp.TintColor == Color.White);
            Debug.Assert(texProp.Visible);
            //Debug.Assert(texProp.TexturePathRelativeToContentRoot == Game.Content.RootDirectory);


            //texProp.Name
            //texProp.ID

            this.AssetName = texProp.AssetName;
            this.Image = game.Content.Load<Texture2D>(this.AssetName);

            //gleed2d system has its origin in the top left, but often makes the texture-rects with
            //their origin field to reoiring them to bottom left?
            //even so, make sure they're all corrected for.
            Debug.Assert(texProp.Origin.X == 0.0f);
            Debug.Assert(texProp.Origin.Y == 0.0f);

            this.Position = texProp.Position;

            Log.Instance.Print(DPFCLASS.GLEED2D, "%\t\t\tInit " + this.ToString());
        }

        public override string ToString()
        {
            return base.ToString() + "{{ " + this.Position + "; " + this.AssetName + " }}";
        }


        public static TexturedRectThingy MapThingyToTexturedRectThingy(MapThingy mt)
        {
            TexturedRectThingy trt = mt as TexturedRectThingy;
            Debug.Assert(trt != null);
            return trt;
        }

    }

    public class MapParallaxLayer : System.IComparable
    {
        //rate at which it scrolls on screen
        public float ParallaxScrollScale;

        //list of quads to draw for this layer.
        //not sure about order or Z?
        public IList<TexturedRectThingy> TexturedQuads;

        //how deep this layer is. 0 is closer towards the middle.
        //for a foreground >0 comes towards the player
        //for a background >0 goes into screen
        public int ParallaxDepth;


        public MapParallaxLayer(List<MapThingy> mapItems, int numeral)
        {
#if DEBUG
            Debug.Assert(numeral >= 0);
            //ensure the foreground is only TexturedRectThingy
            var dbgList =
                from item in mapItems
                where item.GetType() != typeof(TexturedRectThingy)
                select item;
            Debug.Assert(dbgList.Count() == 0);
#endif

            this.TexturedQuads = mapItems.ConvertAll<TexturedRectThingy>(new System.Converter<MapThingy, TexturedRectThingy>(TexturedRectThingy.MapThingyToTexturedRectThingy));
            this.ParallaxDepth = numeral;
            this.ParallaxScrollScale = 0.0f;

            Log.Instance.Print(DPFCLASS.GLEED2D, "%\t\t\tInit " + this.ToString());
        }

        int System.IComparable.CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            MapParallaxLayer other = obj as MapParallaxLayer;
            return other.ParallaxDepth - this.ParallaxDepth;
        }

        public override string ToString()
        {
            string fmt = String.Format("{0}{{ Z:{1} ScrollScale:{2} [{3}] }}", base.ToString(), this.ParallaxDepth,
                this.ParallaxScrollScale, string.Join<TexturedRectThingy>(" ;", this.TexturedQuads));
            return fmt;
        }
    }

    //this hierarchy and the names are terrible.
    public interface IXZPolygon
    {
    }

    public class ClipTriangle : MapThingy, IXZPolygon
    {
        public Vector3[] Vertices = new Vector3[3];

        //for use with sprite batch, rather than screenSpace? -- ie origin adjusts
        //refactor with identical to Dude.CalcScreenPos?
        private Vector2[] CalcScreenPos(Map levelMap)
        {

            Vector2[] ssVerticies = new Vector2[3];


            //this can be done with map or whatever.
            //this.Vertices.Select<Vector3, Vector2>(v => newVector2(v.X, -WorldZToScreenY(v)))

            for (int i = 0; i < this.Vertices.Length; i++)
            {
                //Vector3 worldVert in this.Vertices
                Vector3 worldVert = this.Vertices[i];
                float fScreenY = levelMap.WorldZToScreenY(worldVert.Z);

                Debug.Assert(System.Math.Truncate(fScreenY) == fScreenY);


                Debug.Assert(worldVert.Y == 0.0f);
                //world space, Y is height, so bigger Y means obejcts are in the air, etc, so they should go 'up' the screen
                //screenspace Y=0 is top and Y=whatever is bottom.
                fScreenY -= worldVert.Y;

                //not sure if this assert is useful -- will be moving in float units in Y axis?
                Debug.Assert(System.Math.Truncate(fScreenY) == fScreenY);

                //todo should return some kind of Vector2i ?
                int iScreenY = (int)fScreenY;
                int iScreenX = (int)worldVert.X;

                ssVerticies[i] = new Vector2((float)iScreenX, (float)iScreenY);

            }

            return ssVerticies;

        }

        public void Draw(Map levelMap, Drawing drawing, SpriteBatch spriteBatch)
        {
            Vector2[] spriteSpaceCoords = this.CalcScreenPos(levelMap);

            string fmt = String.Format("ClipTri.Draw() ssCoords: {0} from {1}",
                                          String.Join<Vector2>(", ", spriteSpaceCoords),
                                          this.ToString());
            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW | DPFMASK_MAP.ZONES, fmt);

            drawing.DrawPolygon(spriteBatch, spriteSpaceCoords, Color.Turquoise, 3);
        }

        public override string ToString()
        {
            //string result = this.Vertices.Aggregate<Vector3, string>("", (str, v) => str + v.ToString() + ", ");
            string result = String.Join<Vector3>(",", this.Vertices);
            return "ClipTriangle:{{ verts:" + result + " }}";
        }
    }

    public class ClipRectangle : MapThingy, IXZPolygon
    {
        static uint class_id = 0;

        uint id;

        //probably want to use some kind of Vec3 rectangle, or BoundingBox or something?
        public Rectangle XZPlane;
        public float YPosition;


        public ClipRectangle()
        {
            this.id = ClipRectangle.class_id++;
        }


        //gets the bottom face of the bounding box -- the one touching the "floor"
        internal Rectangle CalcScreenPos(Map levelMap)
        {
            float fScreenY = levelMap.WorldZToScreenY(this.XZPlane.Y / Utility.INT_RECT_SUCKS);

            //wHeightUnadjust: where the 'top' line is. (assuming fScreenY represents the 'bottom') in world space
            float wHeightUnadjust = (this.XZPlane.Y / Utility.INT_RECT_SUCKS) + (this.XZPlane.Height / Utility.INT_RECT_SUCKS);
            float fScreenHeightUnadjust = levelMap.WorldZToScreenY(wHeightUnadjust);
            float fScreenHeight = fScreenHeightUnadjust - fScreenY;
            //Debug.Assert(this.XZPlane.Y == 0.0f);
            //world space, Y is height, so bigger Y means obejcts are in the air, etc, so they should go 'up' the screen
            //screenspace Y=0 is top and Y=whatever is bottom.
            fScreenY -= this.YPosition;

            //XZPlane.

            //argh no float rectangle
            //Debug.Assert(System.Math.Truncate(fScreenY) == fScreenY);
            //Debug.Assert(System.Math.Truncate(fScreenHeight) == fScreenHeight);
            int iScreenY = (int)fScreenY;
            int iScreenHeight = (int)fScreenHeight;

            return new Rectangle(this.XZPlane.X, iScreenY, this.XZPlane.Width, iScreenHeight);
        }

        internal void Draw(Map levelMap, Drawing drawing, SpriteBatch spriteBatch)
        {
            Rectangle ssRect = this.CalcScreenPos(levelMap);

            string fmt = String.Format("ClipRect.Draw() ssRect {0} from {1}", ssRect, this.ToString());
            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW | DPFMASK_MAP.ZONES, fmt);


            Color[] cols = {Color.Red,
                          Color.Green,
                          Color.Yellow,
                         Color.Orange,
                         Color.Khaki};

            Color c = cols[this.id % cols.Length];
            
            
            drawing.DrawBox(spriteBatch, ssRect, c, 4);
        }

        public override string ToString()
        {
            //string result = this.Vertices.Aggregate<Vector3, string>("", (str, v) => str + v.ToString() + ", ");
            return "ClipRectangle:{{ rect:" + XZPlane.ToString() + " Y:" + YPosition + " }}";
        }

    }


    public struct WPYExtents
    {
        public float YTop;
        public float YBottom;

        public override string ToString()
        {
            return base.ToString() + String.Format(" YExtents: Top:{0} Bottom:{1}", this.YTop, this.YBottom);
        }
    }

    //todo -- should visible prims simply be visible, or can they contain clip data?
    public class WalkPlane
    {
        public IList<TexturedRectThingy> BackgroundObjects;
        public IList<IXZPolygon> Zones;
        public WPYExtents YExtents;

        public WalkPlane()
        {
            this.BackgroundObjects = new List<TexturedRectThingy>();
            this.Zones = new List<IXZPolygon>();
        }

        //used during WalkPlane construction : maybe put in MapUtilities, as it won't be used often?
        //it's also a bit crap as it takes  list of MapThingy and then has to type sniff.
        //collection of visible prims
        //collection of non-visible prims?
        public void AddMapItemsToWalkPlane(IList<MapThingy> mapItems)
        {
            //could use mapItems.ofType?
            foreach (var item in mapItems)
            {
                if (item.GetType() == typeof(TexturedRectThingy))
                {
                    Debug.Assert(!(item is IXZPolygon));
                    TexturedRectThingy texRect = item as TexturedRectThingy;
                    Debug.Assert(texRect != null);
                    this.BackgroundObjects.Add(texRect);
                }
                else if (item is IXZPolygon)
                {
                    Debug.Assert(!(item.GetType() == typeof(TexturedRectThingy)));
                    IXZPolygon clipPoly = item as IXZPolygon;
                    Debug.Assert(clipPoly != null);
                    this.Zones.Add(clipPoly);
                }
                else
                {
                    throw new MapLoadException("Central layer can't current cope with:" + item);
                }
            }
        }

        public override string ToString()
        {
            /*
            string bgobjs = String.Join<TexturedRectThingy>(", ", this.BackgroundObjects);
            string zones = String.Join<IXZPolygon>(", ", this.Zones);
            string yextents = this.YExtents.ToString();
            string fmt = String.Format("{{ BGObjs:[ {0} ], Zones:[ {1} ], YExtents:[ {2} ] }}", bgobjs, zones, yextents);
             */
            string fmt = String.Format("{{ BGObjs:[ {0} ], Zones:[ {1} ], YExtents:[ {2} ] }}",
                this.BackgroundObjects.Count, this.Zones.Count, this.YExtents);
            return base.ToString() + fmt;
        }

    }

    public class Map
    {
        //[0] is closest to walk plane
        public List<MapParallaxLayer> BackgroundLayers;
        public WalkPlane WalkPlane;
        //[0] is closest to walk plane
        public List<MapParallaxLayer> ForegroundLayers;


        public Game1 Game1;


        //convert from world Z to screen Y
        //This should probably be in a matrix somehow.
        public static float WorldZToScreenY(WPYExtents YExtents, float worldZ)
        {
            return MathHelper.Lerp(YExtents.YTop, YExtents.YBottom, worldZ);
        }

        //todo : should return int?
        //convert from world Z to screen Y
        //This should probably be in a matrix somehow.
        public float WorldZToScreenY(float worldZ)
        {
            //push into vshader?
            //Debug.Assert(worldZ >= -0.0f);
            //Debug.Assert(worldZ <= 1.0f);
            return WorldZToScreenY(this.WalkPlane.YExtents, worldZ);
        }


        //convert from screen Y to world Z 
        //This should probably be in a matrix somehow.
        public static float ScreenYToWorldZ(WPYExtents YExtents, float screenY)
        {
            //screenY should really be an int.
            Debug.Assert(System.Math.Truncate(screenY) == screenY);

            float YBottom = YExtents.YBottom;
            float YTop = YExtents.YTop;
            float result = (screenY - YTop) / (YBottom - YTop);
            return result;
        }

        //convert from screen Y to world Z 
        //This should probably be in a matrix somehow.
        public float ScreenYToWorldZ(float screenY)
        {
            Debug.Assert(screenY >= this.WalkPlane.YExtents.YBottom);
            Debug.Assert(screenY <= this.WalkPlane.YExtents.YTop);
            return ScreenYToWorldZ(this.WalkPlane.YExtents, screenY);
        }


        Drawing drawing;
        public Map(Game1 game)
        {

            this.drawing = new Drawing(game);

            this.BackgroundLayers = new List<MapParallaxLayer>();
            this.ForegroundLayers = new List<MapParallaxLayer>();
            this.WalkPlane = new WalkPlane();
            this.Game1 = game;
        }






        internal void Draw()
        {
            //Variables specifiy where the background, foreground and walk plane exist in the
            //Z area.
            const float bgZMax = 1.0f;
            const float bgZMin = bgZMax - 0.25f;

            const float wpZMax = bgZMin;
            const float wpZMin = wpZMax - 0.5F;

            const float fgZMax = wpZMin;
            const float fgZMin = 0.0f;



            SpriteBatch spriteBatch = this.Game1.SpriteBatch;

            spriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.Opaque, //alpha test used
                SamplerState.PointClamp,
                DepthStencilState.Default,
                RasterizerState.CullNone,
                this.Game1.MyAlphaTest);

            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, Log.DPF_LVL_0, "Map.Draw() entry");

            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, String.Format("drawing {0} bg layers",
                                                                this.BackgroundLayers.Count));
            if (this.BackgroundLayers.Count > 0)
            {
                //float fMax = this.BackgroundLayers[this.BackgroundLayers.Count - 1].ParallaxDepth;
                //float fMin = this.BackgroundLayers[0].ParallaxDepth;
                float fMin = this.BackgroundLayers[this.BackgroundLayers.Count - 1].ParallaxDepth;
                float fMax = this.BackgroundLayers[0].ParallaxDepth;
                Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t minZ:{0} maxZ:", fMin, fMax);
                foreach (var bg in this.BackgroundLayers)
                {
                    float fZ = Utility.RangeConvert(bg.ParallaxDepth, fMin, fMax, bgZMin, bgZMax);

                    Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t drawing {0} @ Z:{1}", bg, fZ);

                    // would need to sort these in some kind of order as well.
                    Debug.Assert(bg.TexturedQuads.Count == 1);
                    foreach (var tex in bg.TexturedQuads)
                    {
                        Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t\t draw tex" + tex.AssetName);
                        spriteBatch.Draw(tex.Image, tex.Position, null, Color.White, 0.0f, Vector2.Zero,
                            Vector2.One, SpriteEffects.None, fZ);
                    }
                }
            }

            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "drawing wpbg " + this.WalkPlane);
            foreach (var wpbg in this.WalkPlane.BackgroundObjects)
            {
                float fZ = Utility.RangeConvert(0.5f, 0.0f, 1.0f, wpZMin, wpZMax);

                Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t drawing wpbg={0} z={1}", wpbg, fZ);

                spriteBatch.Draw(wpbg.Image, wpbg.Position, null, Color.White, 0.0f, Vector2.Zero,
                    Vector2.One, SpriteEffects.None, fZ);
            }

            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, String.Format("drawing {0} fg layers",
                                                                    this.ForegroundLayers.Count));
            if (this.ForegroundLayers.Count > 0)
            {
                //0 is closet to walk plane, so we want to pretend the range is the other way around.
                float fMax = this.ForegroundLayers[this.ForegroundLayers.Count - 1].ParallaxDepth;
                float fMin = this.ForegroundLayers[0].ParallaxDepth;
                Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t minZ:{0} maxZ:", fMin, fMax);
                foreach (var fg in this.ForegroundLayers)
                {
                    float fZ = Utility.RangeConvert(fg.ParallaxDepth, fMin, fMax, fgZMin, fgZMax);

                    Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t fg={0} z={1}", fg.ParallaxDepth, fZ);

                    // would need to sort these in some kind of order as well.
                    Debug.Assert(fg.TexturedQuads.Count == 1);
                    foreach (var tex in fg.TexturedQuads)
                    {
                        Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\t\tdraw tex" + tex.AssetName);
                        spriteBatch.Draw(tex.Image, tex.Position, null, Color.White, 0.0f, Vector2.Zero,
                            Vector2.One, SpriteEffects.None, fZ);
                    }
                }
            }



            //draw zones with default state.
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.Opaque, //alpha test used
                SamplerState.PointClamp,
                DepthStencilState.Default,
                RasterizerState.CullNone);
            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "drawing wp zones " + this.WalkPlane);
            foreach (var z in this.WalkPlane.Zones)
            {
                Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, "\tzone: " + z);

                if (z is ClipRectangle)
                {
                    ClipRectangle clipRect = z as ClipRectangle;
                    clipRect.Draw(this, drawing, spriteBatch);
                }
                else if (z is ClipTriangle)
                {
                    ClipTriangle clipTri = z as ClipTriangle;
                    clipTri.Draw(this, drawing, spriteBatch);
                }
                else
                {
                    throw new NotImplementedException("who cares?");
                }

            }
            spriteBatch.End();

            Log.Instance.Print(DPFCLASS.MAP, DPFMASK_MAP.DRAW, Log.DPF_LVL_0, "Map.Draw() exit");
        }
    }

    [System.Serializable]
    public class MapLoadException : System.Exception
    {
        public MapLoadException() { }
        public MapLoadException(string message) : base(message) { }
        public MapLoadException(string message, System.Exception inner) : base(message, inner) { }
        protected MapLoadException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }


    //this Background object is pretty rubbish and doesn't do much
    public class Background : DrawableGameComponent
    {
        public Map levelMap;

        private readonly Game1 Game1;


        public Background(Game1 game)
            : base(game)
        {
            //should this class take a reference to game1? why not always pass it in?
            Game1 = game;
            this.DrawOrder = GameConstants.DRAW_PRIORITY_BACKGROUND;
            this.UpdateOrder = GameConstants.UPDATE_PRIORITY_BACKGROUND;
            this.Enabled = false;
            this.Visible = true;
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            this.levelMap = new Map(this.Game1);

            using (System.IO.Stream stream = TitleContainer.OpenStream("Content/test_level_boxingring.gleed"))
            {
                System.Xml.Linq.XElement xml = System.Xml.Linq.XElement.Load(stream);
                Gleed2D.InGame.Level gleedLevel = Gleed2D.InGame.LevelLoader.Load(xml);
                this.levelMap.InitMapFromGleed2D(gleedLevel);
            }

        }


        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            this.levelMap.Draw();

        }
    }

}
