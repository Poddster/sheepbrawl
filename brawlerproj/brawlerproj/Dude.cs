﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

using Microsoft.Xna.Framework.Input;

namespace brawlerproj
{
    public class Dude : DrawableGameComponent
    {
        private Texture2D SingleFrame { get; set; }

        //position of feet
        Vector3 Position; // world coordinates
        Game1 Game1;


        public Dude(Game1 game1)
            : base(game1)
        {
            this.DrawOrder = GameConstants.DRAW_PRIORITY_BACKGROUND;
            this.UpdateOrder = GameConstants.UPDATE_PRIORITY_BACKGROUND;
            this.Enabled = true; //call Update
            this.Visible = true; //call Draw
            this.Position = new Vector3(400f, 0.0f, 0.5f);
            this.Game1 = game1;
        }

        protected override void OnEnabledChanged(object sender, System.EventArgs args)
        {
            base.OnEnabledChanged(sender, args);
        }

        protected override void LoadContent()
        {
            this.SingleFrame = this.Game.Content.Load<Texture2D>("player");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            this.Game1.SpriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.Opaque, //alpha test used
                SamplerState.PointClamp,
                DepthStencilState.Default,
                RasterizerState.CullNone,
                this.Game1.MyAlphaTest);


            Vector2 screenPos = CalcScreenPos();
            this.Game1.SpriteBatch.Draw(this.SingleFrame, screenPos, null, Color.White, 0.0f, Vector2.Zero,
                                    Vector2.One, SpriteEffects.None, 0.5f);
            this.Game1.SpriteBatch.End();
            base.Draw(gameTime);
        }

        //for use with sprite batch, rather than screenSpace? -- ie origin adjusts
        private Vector2 CalcScreenPos()
        {
            float screenY = this.Game1.Background.levelMap.WorldZToScreenY(this.Position.Z);

            //world space, Y is height, so bigger Y means obejcts are in the air, etc, so they should go 'up' the screen
            //screenspace Y=0 is top and Y=whatever is bottom.
            screenY -= this.Position.Y;

            // spritebatch has sprites origin in top left, so need to adjust screen post to reorigin at feet position,
            //which is what this.Position represents
            screenY -= this.SingleFrame.Height;

            Vector2 screenPos = new Vector2(this.Position.X, screenY);
            return screenPos;
        }


        private KeyboardState oldState;
        public override void Update(GameTime gameTime)
        {
            KeyboardState currentState = Keyboard.GetState();
            Vector3 newPos = this.Position;

            const float X_MOV_VAL = 3;
            const float Z_MOV_VAL = 0.01f;


            if (oldState != currentState)
            {
                Keys[] ka = currentState.GetPressedKeys();
                if (ka.Length > 0)
                {
                    Log.Instance.Print(DPFCLASS.DUDE, "{0,8:x8}:{2}:{1}", currentState.GetHashCode(),
                        System.String.Join<Keys>(", ", ka),
                        ka[0].GetHashCode());
                }
            }


            if (currentState.IsKeyDown(Keys.W) ||
                currentState.IsKeyDown(Keys.Up))
            {
                newPos.Z -= Z_MOV_VAL;
            }
            else if (currentState.IsKeyDown(Keys.S) ||
                currentState.IsKeyDown(Keys.Down))
            {
                newPos.Z += Z_MOV_VAL;
            }

            if (currentState.IsKeyDown(Keys.A) ||
                currentState.IsKeyDown(Keys.Left))
            {
                newPos.X -= X_MOV_VAL;
            }
            else if (currentState.IsKeyDown(Keys.D) ||
                currentState.IsKeyDown(Keys.Right))
            {
                newPos.X += X_MOV_VAL;
            }


            newPos.Z = MathHelper.Clamp(newPos.Z, 0.0f, 1.0f);
            Log.Instance.Print(DPFCLASS.DUDE, "dude.newPos={0}", newPos);
            this.Position = newPos;


            base.Update(gameTime);

            oldState = currentState;
        }


    }
}

