﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Gleed2D;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace brawlerproj
{
    public static class Utility
    {
        public const float INT_RECT_SUCKS = 10000;
        public static string DictToString<TKey, TValue>(this IDictionary<TKey, TValue> items)
        {
            return DictToString(items, null);
        }
        public static string DictToString<TKey, TValue>(this IDictionary<TKey, TValue> items, string format)
        {
            format = string.IsNullOrEmpty(format) ? "{0}='{1}' " : format;
            if (items == null || items.Count == 0)
            {
                return "empty";
            }
            return items.Aggregate(new StringBuilder(), (sb, kvp) => sb.AppendFormat(format, kvp.Key, kvp.Value)).ToString();
        }

        //Todo: is it possible to rearrange all of that to be x * somenumber ? That way we can precompute
        // the somenumber and only apply that to all values in a range, rather than redo the calculation
        //for each number
        public static float RangeConvert(float x, float inMin, float inMax, float outMin, float outMax)
        {
            float a = (x - inMin) / (inMax - inMin);
            return (a * (outMax - outMin)) + outMin;
        }
    }



    internal enum DPFCLASS : uint
    {
        MESSAGE,
        ERROR,
        WARN,
        //map, render, dude, entity etc etc.
        MAP,
        DUDE,
        GLEED2D // should probably be MAP and a gleed2d entry in flags?
    }

    [Flags]
    internal enum DPFMASK_MAP : uint
    {
        DRAW = 0x1,
        UPDATE = 0x2,
        ZONES = 0x4,
    }

    [Flags]
    internal enum DPFMASK_DUDE : uint
    {
        DRAW = 0x1,
        UPDATE = 0x2,
    }

    //used pass 'all' value between bridging overloads.
    [Flags]
    enum DPFMASK_DBG : uint
    {
        ALL = 0xFFFFFFFF,
    }


    internal sealed class Log
    {
        public const int DPF_LVL_ALL = -1;
        public const int DPF_LVL_0 = 0;
        public const int DPF_LVL_1 = 1;

        //todo: release print? #ifdef debug?
        //todo: print to file?

        //use Log.Assert instead of debug.assert?

        //rename instance to 'dpf'?
        private static readonly Log instance = new Log();

        //terrible camel case used to make sure I don't cast auiDBGClasses[4] to (int) instead of uint.
        //Probably will never cause a bug
        //0 = most important, int_max = least important, etc.
        private int iDBGLevel;
        private uint[] auiDBGClasses;


        private Log()
        {
            Type enumType = Type.GetType("brawlerproj.DPFCLASS");
            int numElementsInEnum = enumType.GetEnumValues().Length;
            //Type underType = enumType.GetEnumUnderlyingType();
            //this.dbgClasses = Array.CreateInstance(underType, numElementsInEnum);

            this.auiDBGClasses = new uint[numElementsInEnum];
            this.iDBGLevel = DPF_LVL_0;
        }

        internal static Log Instance
        {
            get
            {

                return instance;
            }
        }

        internal void Print(string format)
        {
            this.Print(format, new object[0]);
        }
        internal void Print(DPFCLASS eClass, string format)
        {
            this.Print(eClass, format, new object[0]);
        }

        internal void Print(DPFCLASS eClass, Enum uDPFMask, string format)
        {
            this.Print(eClass, uDPFMask, format, new object[0]);
        }

        internal void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format)
        {
            this.Print(eClass, eDPFMask, level, format, new object[0]);
        }



        internal void Print(string format, params object[] args)
        {
            this.Print(DPFCLASS.MESSAGE, format, args);
        }

        internal void Print(DPFCLASS eClass, string format, params object[] args)
        {
            //defauly mask of ALL
            this.Print(eClass, DPFMASK_DBG.ALL, format, args);
        }

        internal void Print(DPFCLASS eClass, Enum eDPFMask, string format, params object[] args)
        {
            //default level of -1
            this.Print(eClass, eDPFMask, DPF_LVL_ALL, format, args);
        }




        //level: 0 = most important, int_max = least important, etc.
        internal void Print(DPFCLASS eClass, Enum eDPFMask, int level, string format, params object[] args)
        {
            Debug.Assert(Enum.IsDefined(eClass.GetType(), eClass));
            //Debug.Assert((uint) eClass < this.auiDBGClasses.GetLength(0));
            uint uDPFMask = Convert.ToUInt32(eDPFMask);


            if (level <= this.iDBGLevel &&
                ((this.auiDBGClasses[(uint)eClass] & uDPFMask) != 0x0))
            {
                format = eClass.ToString() + ": " + format;
                if (args.Length > 0)
                {
                    Debug.Print(format, args);
                }
                else
                {
                    Debug.Print(format);
                }
            }
        }

    }



}