﻿namespace brawlerproj
{
    public class GameConstants
    {
        public const int DRAW_PRIORITY_BACKGROUND = 0;
        public const int UPDATE_PRIORITY_BACKGROUND = DRAW_PRIORITY_BACKGROUND;

        public const int DRAW_PRIORITY_DUDES = 1;
        public const int UPDATE_PRIORITY_DUDES = DRAW_PRIORITY_DUDES;

    }
}
